// No 5 Array 

function reverseString(str) {
    var currentString = str;
    var newString = '';
   for (let i = str.length - 1; i >= 0; i--) {
     newString = newString + currentString[i];
    }
    
    return newString;
   }
console.log(reverseString("Kasur Rusak")) // kasuR rusaK
console.log(reverseString("SanberCode")) // edoCrebnaS
console.log(reverseString("Haji Ijah")) // hajI ijaH
console.log(reverseString("racecar")) // racecar
console.log(reverseString("I am Sanbers")) // srebnaS ma I